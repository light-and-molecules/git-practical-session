OBJ = test.o
EXE = test

all: $(EXE)

test: $(OBJ)
	$(CC) -o test $(OBJ)

%.o: %.c
	$(CC) -c $<

.PHONY: clean

clean:
	rm -rf $(EXE)
	rm -rf $(OBJ)
